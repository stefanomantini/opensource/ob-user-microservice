# Getting Started

## Configure MongoDB

Change application.conf to the mongoDB instance you're running
```
mongodb.uri = "mongodb://localhost/ob"
```

## Run the app
```
sbt run
```

## For exmples of the API see `routes` in conf