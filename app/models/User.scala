package models

import play.api.libs.json.Json
import reactivemongo.bson._
import play.modules.reactivemongo.json.BSONFormats._

case class User(
  //_id: BSONObjectID = BSONObjectID.generate, ///TODO integrate
  firstName: String,
  lastName: String,
  email:String
)

object User {
  implicit val formatter = Json.format[User]
}
